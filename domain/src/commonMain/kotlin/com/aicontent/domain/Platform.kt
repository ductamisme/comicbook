package com.aicontent.domain

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform