package com.aicontent.resource

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform