pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "ComicBook"
include(":androidApp")
include(":shared")
include(":Common")
include(":common:localization")
include(":common:resource")
include(":common:utils")
include(":data")
include(":data:local")
include(":data:model")
include(":data:remote")
include(":domain")
include(":feature")
//include(":feature:Comic")
include(":feature:comic")
