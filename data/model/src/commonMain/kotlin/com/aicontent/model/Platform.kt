package com.aicontent.model

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform