package com.aicontent.remote

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform